<?php

namespace AppBundle\Utility;

class Url
{
    private $title;
    private $url;
    
    public function __construct($title)
    {
        $this->title = (string) $title;
    }
    
    public function getCleanUrl()
    {
        if(!$this->url){
            $this->clean();
        }
        return $this->url;
    }
    
    private function clean()
    {
		$_search  = ["±","¶","Ľ","ˇ","¦","¬","=","\"", "&#8211;","&#8221;","&#8222;","'",'\"',"ą","ć","ę","ł","ń","ś","ó","ź","ż","Ą","Ć","Ę","Ł","Ń","Ś","Ó","Ź","Ż",",",".","/","!","?",":",";"," - ","-","(",")","&",">","<"];
		$_replace = ["a","s","z","a","s","z","","", "","","","","","a","c","e","l","n","s","o","z","z","A","C","E","L","N","S","O","Z","Z","","","","","","","","-","-","-","-","","",""];
		
        $url = $this->title;
		$url = str_replace($_search, $_replace, $url);
		$url = mb_strtolower($url, 'UTF-8');
        $url = str_replace(' ', '-', trim (strip_tags ($url)));
        $url = preg_replace('/[^0-9a-z\-]+/', '', $url);
        $url = preg_replace('/[\-]+/', '-', $url);
        $url = trim($url, '-');
        
        $this->url = strtolower($url);
    }
}
