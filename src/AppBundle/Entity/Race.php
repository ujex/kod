<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Race
 *
 * @ORM\Table(name="race", uniqueConstraints={@ORM\UniqueConstraint(name="url", columns={"url"})}, indexes={@ORM\Index(name="category_id", columns={"category_id"})})
 * @ORM\Entity
 */
class Race
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=8, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=64, nullable=true)
     */
    private $city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="organiser", type="string", length=256, nullable=true)
     */
    private $organiser;
    
    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text", length=65535, nullable=false)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("\DateTime")
     */
    private $date;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registration", type="datetime", nullable=true)
     */
    private $registration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_lat", type="string", length=16, nullable=true)
     */
    private $gpsLat;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_lng", type="string", length=16, nullable=true)
     */
    private $gpsLng;

    /**
     * @var integer
     *
     * @ORM\Column(name="race_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $raceId;

    /**
     * @var \AppBundle\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="category_id")
     * })
     */
    private $category;


    /**
     * @ORM\OneToMany(targetEntity="File", mappedBy="race")
     */
    protected $files;
    
    /**
     * @ORM\OneToMany(targetEntity="Mailer", mappedBy="race")
     */
    protected $mailers;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }
    
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Race
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Race
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set code
     *
     * @param string $code
     *
     * @return Race
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return Race
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Race
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Race
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set gpsLat
     *
     * @param string $gpsLat
     *
     * @return Race
     */
    public function setGpsLat($gpsLat)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * Get gpsLat
     *
     * @return string
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set gpsLng
     *
     * @param string $gpsLng
     *
     * @return Race
     */
    public function setGpsLng($gpsLng)
    {
        $this->gpsLng = $gpsLng;

        return $this;
    }

    /**
     * Get gpsLng
     *
     * @return string
     */
    public function getGpsLng()
    {
        return $this->gpsLng;
    }

    /**
     * Set registration
     *
     * @param boolean $registration
     *
     * @return Race
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;

        return $this;
    }

    /**
     * Get registration
     *
     * @return boolean
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * Get raceId
     *
     * @return integer
     */
    public function getRaceId()
    {
        return $this->raceId;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Race
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add file
     *
     * @param \AppBundle\Entity\File $file
     *
     * @return Race
     */
    public function addFile(\AppBundle\Entity\File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \AppBundle\Entity\File $file
     */
    public function removeFile(\AppBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add mailer
     *
     * @param \AppBundle\Entity\Mailer $mailer
     *
     * @return Mailer
     */
    public function addMailer(\AppBundle\Entity\Mailer $mailer)
    {
        $this->mailers[] = $mailer;

        return $this;
    }

    /**
     * Remove mailer
     *
     * @param \AppBundle\Entity\Mailer $mailer
     */
    public function removeMailer(\AppBundle\Entity\Mailer $mailer)
    {
        $this->mailers->removeElement($mailer);
    }

    /**
     * Get mailerss
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMailers()
    {
        return $this->mailers;
    }
    
    /**
     * Set city
     *
     * @param string $city
     *
     * @return Race
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set organiser
     *
     * @param string $organiser
     *
     * @return Race
     */
    public function setOrganiser($organiser)
    {
        $this->organiser = $organiser;

        return $this;
    }

    /**
     * Get organiser
     *
     * @return string
     */
    public function getOrganiser()
    {
        return $this->organiser;
    }
}
