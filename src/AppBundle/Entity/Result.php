<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Result
 *
 * @ORM\Table(name="result", indexes={@ORM\Index(name="rally_id", columns={"rally_id"})})
 * @ORM\Entity
 */
class Result
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="lp", type="integer", nullable=false)
     */
    private $lp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * @var string
     *
     * @ORM\Column(name="bib", type="string", length=8, nullable=true)
     */
    private $bib;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=64, nullable=false)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=11, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="team", type="string", length=64, nullable=true)
     */
    private $team;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=16, nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="diff", type="string", length=16, nullable=true)
     */
    private $diff;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="string", length=16, nullable=true)
     */
    private $info;

    /**
     * @var integer
     *
     * @ORM\Column(name="result_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $resultId;

    /**
     * @var \AppBundle\Entity\Rally
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Rally")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rally_id", referencedColumnName="rally_id")
     * })
     */
    private $rally;



    /**
     * Set lp
     *
     * @param boolean $lp
     *
     * @return Result
     */
    public function setLp($lp)
    {
        $this->lp = $lp;

        return $this;
    }

    /**
     * Get lp
     *
     * @return boolean
     */
    public function getLp()
    {
        return $this->lp;
    }

    /**
     * Set pos
     *
     * @param boolean $pos
     *
     * @return Result
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return boolean
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set bib
     *
     * @param string $bib
     *
     * @return Result
     */
    public function setBib($bib)
    {
        $this->bib = $bib;

        return $this;
    }

    /**
     * Get bib
     *
     * @return string
     */
    public function getBib()
    {
        return $this->bib;
    }
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Result
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Result
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Result
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set team
     *
     * @param string $team
     *
     * @return Result
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Result
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set diff
     *
     * @param string $diff
     *
     * @return Result
     */
    public function setDiff($diff)
    {
        $this->diff = $diff;

        return $this;
    }

    /**
     * Get diff
     *
     * @return string
     */
    public function getDiff()
    {
        return $this->diff;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return Result
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Get resultId
     *
     * @return integer
     */
    public function getResultId()
    {
        return $this->resultId;
    }

    /**
     * Set rally
     *
     * @param \AppBundle\Entity\Rally $rally
     *
     * @return Result
     */
    public function setRally(\AppBundle\Entity\Rally $rally = null)
    {
        $this->rally = $rally;

        return $this;
    }

    /**
     * Get rally
     *
     * @return \AppBundle\Entity\Rally
     */
    public function getRally()
    {
        return $this->rally;
    }
}
