<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Player", inversedBy="user")
     * @ORM\JoinTable(name="userplayer",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="player_id", referencedColumnName="player_id")
     *   }
     * )
     * @ORM\OrderBy({"surname" = "ASC", "name" = "DESC"})
     */
    private $player;


    public function __construct()
    {
        parent::__construct();
        $this->player = new \Doctrine\Common\Collections\ArrayCollection();
        $this->roles = array('ROLE_PLAYER');
    }

    
    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return User
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->player[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->player->removeElement($player);
    }

    /**
     * Get player
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayer()
    {
        return $this->player;
    }
}
