<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category", uniqueConstraints={@ORM\UniqueConstraint(name="news_category_url", columns={"ctgurl"})})
 * @ORM\Entity
 */
class Category
{
    /**
     * @var string
     *
     * @ORM\Column(name="ctgname", type="string", length=64, nullable=false)
     */
    private $ctgname;

    /**
     * @var string
     *
     * @ORM\Column(name="ctgurl", type="string", length=64, nullable=false)
     */
    private $ctgurl;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoryId;



    /**
     * Set ctgname
     *
     * @param string $ctgname
     *
     * @return Category
     */
    public function setCtgname($ctgname)
    {
        $this->ctgname = $ctgname;

        return $this;
    }

    /**
     * Get ctgname
     *
     * @return string
     */
    public function getCtgname()
    {
        return $this->ctgname;
    }

    /**
     * Set ctgurl
     *
     * @param string $ctgurl
     *
     * @return Category
     */
    public function setCtgurl($ctgurl)
    {
        $this->ctgurl = $ctgurl;

        return $this;
    }

    /**
     * Get ctgurl
     *
     * @return string
     */
    public function getCtgurl()
    {
        return $this->ctgurl;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }
}
