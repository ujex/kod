<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Page;

class PageController extends Controller {
    
    public function showAction(Request $request)
    {
        $repoPage = $this->getDoctrine()->getRepository('AppBundle:Page');
        $page = $repoPage->findOneBy(
            ['url' => $request->get('url')]
        );

        if (!$page) {
            throw $this->createNotFoundException(
                'No result found for url '.$request->getBaseUrl()
            );
        }

        return $this->render('page/show.html.twig', [
            'page' => $page,
        ]);
    }
}
