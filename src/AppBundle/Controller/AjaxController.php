<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Registration;

class AjaxController extends Controller {
    
    public function showResultAction(Request $request)
    {
        $rally = $this->getDoctrine()->getRepository('AppBundle:Rally')->find($request->get('id'));
        $result = $this->getDoctrine()->getRepository('AppBundle:Result')->findBy([
            'rally' => $request->get('id')
        ]);
        
        return $this->render('ajax/showResult.html.twig', [
            'result' => $result,
            'rally' => $rally
        ]);
    }


    public function playerRegisterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $is_registration = $em->getRepository('AppBundle:Registration')->findOneBy([
            'player' => $request->get('player'),
            'race' => $request->get('race'),
        ]);
        
        if($request->get('checked') && !$is_registration){
            $player = $em->getRepository('AppBundle:Player')->find($request->get('player'));
            $race = $em->getRepository('AppBundle:Race')->find($request->get('race'));
            $user = $this->getUser();

            $registration = new Registration();
            $registration->setPlayer($player);
            $registration->setRace($race);
            $registration->setTeam($player->getTeam());
            $registration->setUser($user);
            $registration->setDateAdd(new \DateTime('now'));
            $registration->setConfirm(1);
            $em->persist($registration);
            $em->flush();
        }
        if(!$request->get('checked') && $is_registration){
            $em->remove($is_registration);
            $em->flush();
        }
        
        $data = [
            'success' => true,
        ];
        return new JsonResponse($data);
    }

    public function playerRaceListAction(Request $request)
    {
        $user = $this->getUser();

        $race_id = addslashes($request->get('rid'));
        $user_id = addslashes($user->getId());
        $conn = $this->get('database_connection');
        $sql = "SELECT p.*,r.player_id as rpid 
            FROM player p 
            INNER JOIN userplayer up ON p.player_id = up.player_id 
            INNER JOIN user u ON u.id = up.user_id 
            LEFT JOIN registration r ON (r.player_id = p.player_id AND r.race_id = $race_id) 
            WHERE u.id = $user_id 
            ORDER BY p.surname ASC
        ";
        $player = $conn->fetchAll($sql);
        
        return $this->render('ajax/playerRaceList.html.twig', [
            'rid' => $request->get('rid'),
            'player' => $player
        ]);
    }
    
    public function userPlayerListAction(Request $request)
    {
        $user_id = addslashes($request->get('id'));
        $conn = $this->get('database_connection');
        $sql = "SELECT p.*, t.name as team_name
            FROM player p 
            INNER JOIN userplayer up ON p.player_id = up.player_id 
            LEFT JOIN team t ON t.team_id = p.team_id
            WHERE up.user_id = $user_id 
            ORDER BY p.date_add ASC
        ";
        $player = $conn->fetchAll($sql);
        
        return $this->render('ajax/userPlayerList.html.twig', [
            'player' => $player
        ]);
    }
    
    public function playerRaceAllAction(Request $request)
    {
        $race_id = addslashes($request->get('rid'));
        $conn = $this->get('database_connection');
        $sql = "SELECT p.*,r.player_id as rpid, r.date_add as dateadd, t.name as team_name, u.email, u.id as uid
            FROM player p 
            INNER JOIN registration r ON (r.player_id = p.player_id AND r.race_id = $race_id) 
            INNER JOIN team t ON (r.team_id = t.team_id)
            INNER JOIN user u ON (r.user_id = u.id)
            WHERE 1=1 
            ORDER BY r.date_add, t.name ASC, p.surname ASC
            

        ";
        $player = $conn->fetchAll($sql);
        
        return $this->render('ajax/playerRaceAll.html.twig', [
            'player' => $player
        ]);
    }
    
    public function playerChangeTeamAction(Request $request)
    {
        $pid = $request->get('pid');
        $new_team = $request->get('new_team');
        $user = $this->getUser();
        
        $body  = "Zmienić klub zawodnikowi: " . $pid . "\n";
        $body .= "Nowy klub: " . $new_team . "\n";
        $body .= "Zmieniający: " . $user->getId() . "\n";
        
        $message = \Swift_Message::newInstance()
            ->setSubject('Zmiana klubu')
            ->setFrom('biuro@e-zawody.pl')
            ->setTo('lukaszkowalcze@gmail.com')
            ->setBody($body)
        ;
        $this->get('mailer')->send($message);

        $data = [
            'success' => true,
        ];
        return new JsonResponse($data);
    }
}
