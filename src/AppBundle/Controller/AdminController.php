<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Race;
use AppBundle\Entity\File;
use AppBundle\Form\RaceType;
use AppBundle\Form\MyfileType;
use Swift_Attachment;

class AdminController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->from('AppBundle:Race', 'r')
            ->select("r")
            ->orderBy("r.date", "DESC")
        ;
        $race = $query->getQuery()->getResult();
        
        return $this->render('admin/index.html.twig', [
            'race' => $race
        ]);
    }

    public function addAction(Request $request)
    {
        $race  = new Race();
        $form = $this->createForm(new RaceType(), $race);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $race->setUrl((new \AppBundle\Utility\Url($race->getName()))->getCleanUrl().'-'.substr(md5(time()), 7, 3));
                $em = $this->getDoctrine()->getManager();
                $em->persist($race);
                $em->flush();
                return $this->redirectToRoute('site_admin_index');
            }
        }
        return $this->render('admin/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function editAction(Request $request)
    {
        $repoRace = $this->getDoctrine()->getRepository('AppBundle:Race');
        $race = $repoRace->find($request->get('id'));
        $form = $this->createForm(new RaceType(), $race);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($race);
                $em->flush();
                return $this->redirectToRoute('site_admin_index');
            }
        }
        if($race->getCode()){
            \PHPQRCode\QRcode::png("http://e-zawody.pl/result-" . $race->getCode(), "qr_code/res-".$race->getCode().".png", 'L', 8, 2);
        }
        
        return $this->render('admin/edit.html.twig', [
            'form' => $form->createView(),
            'race' => $race
        ]);
    }
    
    public function editFileAction(Request $request)
    {
        $repoRace = $this->getDoctrine()->getRepository('AppBundle:Race');
        $race = $repoRace->find($request->get('id'));
        $file = new File();
        $form = $this->createForm(new MyfileType(), $file);
                
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                
                $f = $form['path']->getData();
                $upload_dir = __DIR__.'/../../../web/public';
                if($file->getFtype() == 1){
                    $path = '/wyniki/';
                }else{
                    $path = '/regulamin/';
                }
                $path .= date('Y').'/';
                $upload_dir .= $path;
                $path .= $f->getClientOriginalName();
                $f->move($upload_dir, $f->getClientOriginalName());
                $file->setPath($path);
                $file->setRace($race);
                $file->setDateAdd(new \DateTime('now'));

                $em->persist($file);
                $em->flush();
                
                $mailer = $race->getMailers();
                foreach($mailer as $mail){
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Results of '.$race->getName())
                        ->setFrom(['biuro@e-zawody.pl' => 'e-zawody.pl'])
                        ->setTo($mail->getEmail())
                        ->setBody(
                            $this->renderView('emails/new_file.html.twig', ['r' => $race]),
                            'text/html'
                        )
                        ->attach(Swift_Attachment::fromPath($upload_dir.$f->getClientOriginalName()))
                    ;
                    $this->get('mailer')->send($message);
                }

                return $this->redirectToRoute('site_admin_editFile', ['id' => $race->getRaceId()]);
            }
        }
        
        $repoFile = $this->getDoctrine()->getRepository('AppBundle:File');
        $files = $repoFile->findBy([
            'race' => $race->getRaceId(),
            'ftype' => 1
        ],[
            'dateAdd' => 'ASC'
        ]);
        $reg = $repoFile->findBy([
            'race' => $race->getRaceId(),
            'ftype' => 2
        ],[
            'dateAdd' => 'ASC'
        ]);
        
        if($race->getCode()){
            \PHPQRCode\QRcode::png("http://e-zawody.pl/result-" . $race->getCode(), "qr_code/res-".$race->getCode().".png", 'L', 8, 2);
        }
        
        return $this->render('admin/editFile.html.twig', [
            'form' => $form->createView(),
            'race' => $race,
            'reg' => $reg,
            'files' => $files
        ]);
    }

    public function delAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repoNews = $em->getRepository('AppBundle:Race');
        $news = $repoNews->findOneBy([
            'raceId' => $request->get('id')
        ]);
        $news->setStatus(0);
        $em->flush();
        return $this->redirectToRoute('site_admin_index');
    }
    
    public function userListAction()
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUsers();
        
        return $this->render('admin/userList.html.twig', [
            'user' => $user,
        ]);
    }
}