<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\PlayeraddType;
use AppBundle\Entity\Player;
use AppBundle\Entity\User;

class UserController extends Controller
{
    public function mergeAction(Request $request)
    {
        $player = new Player();
        $form = $this->createForm(new PlayeraddType(), $player);
        
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
            	$user = $this->getUser();
                $em = $this->getDoctrine()->getManager();
                $repoPlayer = $em->getRepository('AppBundle:Player');
                $player_search = $repoPlayer->findOneBy([
                    'name' => $player->getName(),
                    'surname' => $player->getSurname(),
                    'dateBirth' => $player->getDateBirth()
                ]);
                if($player_search){
                	$player = $player_search;
                }else{
                	if(!$player->getTeam()){
                		$team = $em->getRepository('AppBundle:Team')->find(1);
                		$player->setTeam($team);
                	}
                	$player->setDateAdd(new \DateTime());
                	$em->persist($player);
                	$em->flush();
                }
                // @todo Team
                if(!$user->getPlayer()->contains($player)){
                	$user->addPlayer($player);
                	$player->addUser($user);
                	$em->persist($user);
                	$em->persist($player);
                	$em->flush();
                }
                return $this->redirectToRoute('panel_user_list');
            }
        }
        return $this->render('user/merge.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    public function unmergeAction($player_id)
    {
        $user = $this->getUser();
        $conn = $this->get('database_connection');
        $sql = "DELETE 
            FROM userplayer 
            WHERE player_id=".$player_id." 
            AND user_id=".$user->getId()."
        ";
        $conn->executeQuery($sql);
        return $this->redirectToRoute('panel_user_list');
    }


    public function listAction()
    {
    	$user = $this->getUser();
    	
    	return $this->render('user/list.html.twig', [
    		'player' => $user->getPlayer()
    	]);
    }
    
    public function upcAction()
    {
//        $em = $this->getDoctrine()->getManager();
//        $query = $em->createQueryBuilder()
//            ->from('AppBundle:Race', 'r')
//            ->select("r")
//            ->where("r.status = :status")
//            ->andWhere("r.date > :now")
//            ->orderBy("r.date", "ASC")
//            ->setParameters(['status' => 1, 'now' => new \DateTime('now')]);
//        $race_upc = $query->getQuery()->getResult();
        $user = $this->getUser();
        $user_id = addslashes($user->getId());
        $sql = "SELECT r.*, IF(r.registration > NOW(), 1, 0) AS is_reg,
            (SELECT id FROM user u JOIN registration re ON re.user_id=u.id WHERE re.race_id=r.race_id AND u.id=$user_id LIMIT 1) AS is_sign
            FROM race r 
            WHERE r.status=1 
            AND r.date > NOW()
            ORDER BY r.date ASC
        ";
        $race_upc = $this->get('database_connection')->fetchAll($sql);

        return $this->render('user/upc.html.twig',[
            'race_upc' => $race_upc
        ]);
    }
}
