<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Rally;
use AppBundle\Entity\Result;

class ApiController extends Controller {
    
    public function playerRaceAllAction(Request $request)
    {
        $race_id = addslashes($request->get('id'));
        $token = substr(md5($race_id), 7, 13);
        if($token != $request->get('token')){
            return new JsonResponse([
                'success' => false,
                'msg' => "Access denied",
                'count' => 0,
                'data' => null
            ]);
        }
        $conn = $this->get('database_connection');
        $sql = "SELECT p.*, 
                t.name as team_name,
                r.date_add, r.confirm, r.comment, r.user_id
            FROM player p 
            INNER JOIN registration r ON (r.player_id = p.player_id AND r.race_id = $race_id) 
            INNER JOIN team t ON (r.team_id = t.team_id)
            WHERE 1=1 
            ORDER BY r.date_add, t.name ASC, p.surname ASC
        ";
        
        $player = $conn->fetchAll($sql);
        
        return new JsonResponse([
            'success' => true,
            'msg' => "Registered Players",
            'count' => count($player),
            'data' => $player
        ]);
    }
    
    public function updateResultAction(Request $request)
    {
        $res_nag = json_decode($request->get('res_nag'));
        $res_pos = json_decode($request->get('res_pos'));
        if(!isset($res_nag->race_id)){
            throw new \Exception('Param err');
        }
        $race_id = addslashes($res_nag->race_id);
        $token = substr(md5($race_id), 7, 13);
        if($token != $request->get('token')){
            return new JsonResponse([
                'success' => false,
                'msg' => "Access denied",
                'count' => 0,
                'data' => null
            ]);
        }
        $race = $this->getDoctrine()->getRepository('AppBundle:Race')->find($race_id);
        
        $rally = $this->getDoctrine()->getRepository('AppBundle:Rally')->findOneBy([
            'race' => $race,
            'name' => $res_nag->name
        ]);
        if(!$rally){
            $rally  = new Rally();
            $rally->setDateAdd(new \DateTime('now'));
        }
        $rally->setName($res_nag->name);
        $rally->setLength($res_nag->length);
        $rally->setSpeed($res_nag->speed);
        $rally->setInfo($res_nag->info);
        $rally->setStatus($res_nag->status);
        $rally->setDateMod(new \DateTime('now'));
        $rally->setRace($race);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($rally);
        $em->flush();
        
        $results = $this->getDoctrine()->getRepository('AppBundle:Result')->findBy([
            'rally' => $rally
        ]);

        if($results){
            foreach ($results as $r){
                $em->remove($r);
            }
            $em->flush();
        }
        foreach($res_pos as $p){
            $result = new Result();
            $result->setRally($rally);
            $result->setLp($p->lp);
            $result->setPos($p->pos);
            $result->setBib($p->bib);
            $result->setName($p->name);
            $result->setSurname($p->surname);
            $result->setCode($p->code);
            $result->setTeam($p->team);
            $result->setResult($p->result);
            $result->setDiff($p->diff);
            $result->setInfo($p->info);
            
            $em->persist($result);
        }
        $em->flush();

        return new JsonResponse([
            'success' => true,
            'msg' => "Results updated successful",
            'count' => count($res_pos),
            'data' => $res_nag
//            'res_pos' => $res_pos
        ]);
    }
}
