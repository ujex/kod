<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{

    public function startAction()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->from('AppBundle:Race', 'r')
            ->select("r")
            ->leftJoin('AppBundle:Category', 'c', 'WITH', 'r.category=c.categoryId')
            ->where("r.status = :status")
            ->andWhere("r.date <= :now")
            ->orderBy("r.date", "DESC")
            ->setMaxResults(10)
            ->setParameters([
                'status' => 1,
                'now' => new \DateTime('now')
            ]);
        $race = $query->getQuery()->getResult();

        return $this->render('default/start.html.twig',[
            'race' => $race
        ]);
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addRoleAction()
    {
        $user = $this->getUser();
        $userManager = $this->get('fos_user.user_manager');
        $user->addRole('ROLE_SUPER_ADMIN');
        $userManager->updateUser($user);
        
        $user->hasRole('ROLE_USER');
    }
}