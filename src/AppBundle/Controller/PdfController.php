<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;

class PdfController extends Controller {
    
    public function confirmRegistrationAction(Request $request)
    {
        $user = $this->getUser();
        $race_id = addslashes($request->get('id'));
        $user_id = addslashes($user->getId());
        
        $race = $this->getDoctrine()->getRepository('AppBundle:Race')
            ->find($race_id);

        $sql = "SELECT p.*,r.player_id as rpid, r.date_add as dateadd, t.name as team_name, u.email, u.id as uid
            FROM player p 
            INNER JOIN registration r ON (r.player_id = p.player_id AND r.race_id = $race_id) 
            INNER JOIN team t ON (r.team_id = t.team_id)
            INNER JOIN user u ON (r.user_id = u.id)
            WHERE u.id = $user_id 
            ORDER BY p.surname ASC
            

        ";
        $player = $this->get('database_connection')->fetchAll($sql);
        
        $pdf = $this->get("white_october.tcpdf")->create();
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('FreeSans', '', 10);
        $pdf->AddPage();
        
        
        $html = $this->render('pdf/confirmRegistration.html.twig', [
            'race' => $race,
            'player' => $player
        ]);
        $file_name = $user_id.$race_id.'_'.time().'.pdf';
        $pdf->writeHTML($html->getContent(), true, false, true, false, '');
        return new StreamedResponse(function () use ($pdf, $file_name) {
            $pdf->Output($file_name, 'D');
        });
    }

    public function registeredPlayerAction(Request $request)
    {
        $race_id = addslashes($request->get('id'));
        
        $race = $this->getDoctrine()->getRepository('AppBundle:Race')
            ->find($race_id);

        $sql = "SELECT p.*,r.player_id as rpid, r.date_add as dateadd, t.name as team_name, u.email, u.id as uid
            FROM player p 
            INNER JOIN registration r ON (r.player_id = p.player_id AND r.race_id = $race_id) 
            INNER JOIN team t ON (r.team_id = t.team_id)
            INNER JOIN user u ON (r.user_id = u.id)
            WHERE 1=1
            ORDER BY r.date_add ASC, p.surname ASC
            

        ";
        $player = $this->get('database_connection')->fetchAll($sql);
        
        $pdf = $this->get("white_october.tcpdf")->create();
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('FreeSans', '', 10);
        $pdf->AddPage();
        
        
        $html = $this->render('pdf/registeredPlayer.html.twig', [
            'race' => $race,
            'player' => $player
        ]);
        $file_name = $race_id.'_'.time().'.pdf';
        $pdf->writeHTML($html->getContent(), true, false, true, false, '');
        return new StreamedResponse(function () use ($pdf, $file_name) {
            $pdf->Output($file_name, 'D');
        });
    }
    
    public function registeredPlayerPublicAction(Request $request)
    {
        $race_id = addslashes($request->get('id'));
        
        $race = $this->getDoctrine()->getRepository('AppBundle:Race')
            ->find($race_id);

        $sql = "SELECT p.*,r.player_id as rpid, r.date_add as dateadd, t.name as team_name, u.email, u.id as uid
            FROM player p 
            INNER JOIN registration r ON (r.player_id = p.player_id AND r.race_id = $race_id) 
            INNER JOIN team t ON (r.team_id = t.team_id)
            INNER JOIN user u ON (r.user_id = u.id)
            WHERE 1=1
            ORDER BY r.date_add ASC, p.surname ASC
            

        ";
        $player = $this->get('database_connection')->fetchAll($sql);
        
        $pdf = $this->get("white_october.tcpdf")->create();
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('FreeSans', '', 10);
        $pdf->AddPage();
        
        
        $html = $this->render('pdf/registeredPlayer.html.twig', [
            'race' => $race,
            'player' => $player
        ]);
        $file_name = $race_id.'_'.time().'.pdf';
        $pdf->writeHTML($html->getContent(), true, false, true, false, '');
        return new StreamedResponse(function () use ($pdf, $file_name) {
            $pdf->Output($file_name, 'D');
        });
    }
}