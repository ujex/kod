<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class RaceController extends Controller {
    
    public function resultAction(Request $request)
    {
        $race = $this->getDoctrine()->getRepository('AppBundle:Race')->findOneBy([
            'code' => $request->get('code')
        ]);
        if (!$race) {
            throw $this->createNotFoundException(
                'No result found for url '.$request->getBaseUrl()
            );
        }
        $rally = $this->getDoctrine()->getRepository('AppBundle:Rally')->findBy([
            'race' => $race->getRaceId(),
            'status' => 1
        ],[
            'dateMod' => 'DESC'
        ]);

        $repoFile = $this->getDoctrine()->getRepository('AppBundle:File');
        $file = $repoFile->findBy([
            'race' => $race->getRaceId(),
            'ftype' => 1
        ],[
            'dateAdd' => 'ASC'
        ]);
        
        return $this->render('race/result.html.twig', [
            'race' => $race,
            'file' => $file,
            'rally' => $rally
        ]);
    }
    
    public function showAction(Request $request)
    {
        $repoNews = $this->getDoctrine()->getRepository('AppBundle:Race');
        $race = $repoNews->findOneBy([
            'url' => $request->get('url')
        ]);
        if (!$race) {
            throw $this->createNotFoundException(
                'No result found for url '.$request->getBaseUrl()
            );
        }
        $rally = $this->getDoctrine()->getRepository('AppBundle:Rally')->findBy([
            'race' => $race->getRaceId(),
            'status' => 1
        ]);
        $repoFile = $this->getDoctrine()->getRepository('AppBundle:File');
        $file = $repoFile->findBy([
            'race' => $race->getRaceId(),
            'ftype' => 1
        ],[
            'dateAdd' => 'ASC'
        ]);
        $reg = $repoFile->findBy([
            'race' => $race->getRaceId(),
            'ftype' => 2
        ],[
            'dateAdd' => 'ASC'
        ]);
        
        return $this->render('race/show.html.twig', [
            'race' => $race,
            'file' => $file,
            'reg'  => $reg,
            'rally' => ($rally ? true : false)
        ]);
    }
    
    public function listAction($category = '')
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->from('AppBundle:Race', 'r')
            ->select("r")
            ->leftJoin('AppBundle:Category', 'c', 'WITH', 'r.category=c.categoryId')
            ->where("r.status = :status")
            ->andWhere("r.date <= :now")
            ->orderBy("r.date", "DESC")
            ->setParameters([
                'status' => 1,
                'now' => new \DateTime('now')
            ]);
        if($category){
            $query->andWhere("c.ctgurl = :category")
                ->setParameter('category', $category);
        }
        $race = $query->getQuery()->getResult();

        return $this->render('race/list.html.twig',[
            'race' => $race
        ]);
    }


    
    public function shortBoxAction($max = 3)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->from('AppBundle:Race', 'r')
            ->select("r")
            ->where("r.status = :status")
            ->andWhere("r.date <= :now")
            ->orderBy("r.date", "DESC")
            ->setMaxResults($max)
            ->setParameters(['status' => 1, 'now' => new \DateTime('now')]);
        $race_last = $query->getQuery()->getResult();
        
        $query->where("r.status = :status")
            ->andWhere("r.date > :now")
            ->orderBy("r.date", "ASC")
            ->setParameters(['status' => 1, 'now' => new \DateTime('now')]);
        $race_upc = $query->getQuery()->getResult();
        
        return $this->render('race/short_box.html.twig',[
            'race_last' => $race_last,
            'race_upc' => $race_upc,
        ]);
    }

}
