<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class MyfileType extends AbstractType
{
	public function buildForm(FormBuilderInterface $b, array $options)
	{
            $b->add('title', 'text', [
                'required' => true
            ])
            ->add('ftype', 'choice', [
                'choices' => [
                    1 => 'Result',
                    2 => 'Regulation',
                ]
            ])
            ->add('path', 'file')
            ->add('save', 'submit', array('label' => 'Add'));
	}

	public function getName()
	{
		return 'race';
	}
}