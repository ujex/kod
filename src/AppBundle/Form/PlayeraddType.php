<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class PlayeraddType extends AbstractType
{
	public function buildForm(FormBuilderInterface $b, array $options)
	{
            $b->add('name', 'text', [
                'label' => 'player.name',
                'translation_domain' => 'form'
            ])
            ->add('surname', 'text', [
                'label' => 'player.surname',
                'translation_domain' => 'form'
            ])
            ->add('dateBirth', 'date', [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'label' => 'player.dateBirth',
                'translation_domain' => 'form'
            ])
            ->add('sex', 'choice', [
                'choices' => [
                    2 => 'player.sexo.m',
                    1 => 'player.sexo.f'
                ],
                'label' => 'player.sex',
                'translation_domain' => 'form'
            ])
            ->add('team', 'entity', [
                'class' => 'AppBundle:Team',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => false,
                'label' => 'player.team',
                'translation_domain' => 'form'
            ])
            ->add('country', 'choice', [
                'choices' => [
                    'POL' => 'player.countryo.pol',
                    'SVK' => 'player.countryo.svk',
                    'CZE' => 'player.countryo.cze',
                    'GER' => 'player.countryo.ger',
                    'UKR' => 'player.countryo.ukr',
                    'ITA' => 'player.countryo.ita'
                ],
                'label' => 'player.country',
                'translation_domain' => 'form'
            ])
            ->add('save', 'submit', [
                'label' => 'player.save',
                'translation_domain' => 'form'
            ]);
	}

	public function getName()
	{
		return 'race';
	}
}
