<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class RaceType extends AbstractType
{
	public function buildForm(FormBuilderInterface $b, array $options)
	{
            $b->add('name', 'text')
            ->add('info', 'textarea', [
                'required' => false
            ])
            ->add('organiser', 'text', [
                'required' => false
            ])
            ->add('city', 'text', [
                'required' => false
            ])
            ->add('code', 'text')
            ->add('date', 'date', [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('registration', 'date', [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm:ss',
                'required' => false
            ])
            ->add('status', 'choice', [
                'choices' => [
                    0 => 'Nieaktywny',
                    1 => 'Aktywny',
                ]
            ])
            ->add('category', 'entity', [
                'class' => 'AppBundle:Category',
                'choice_label' => 'ctgname',
            ])
            ->add('gpsLat', 'text', [
                'required' => false
            ])
            ->add('gpsLng', 'text', [
                'required' => false
            ])
            ->add('save', 'submit', array('label' => 'Save'));
	}

	public function getName()
	{
		return 'race';
	}
}