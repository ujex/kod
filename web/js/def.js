$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    
    $('.confirm').click(function(e) {
        e.preventDefault();
        if (window.confirm("Czy na pewno chcesz wykonać tą akcję?")) {
            location.href = this.href;
        }
    });
});