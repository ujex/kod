<?php

/* 
 * Routing settings
 */

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('site_default_start', new Route('/', [
    '_controller' => 'AppBundle:Default:start',
]));
$collection->add('site_race_show', new Route('/wyniki/{url}.html', [
    '_controller' => 'AppBundle:Race:show',
]));
$collection->add('site_race_result', new Route('/result-{code}', [
    '_controller' => 'AppBundle:Race:result',
]));
$collection->add('site_race_list_param', new Route('/wyniki/{year},{category}/',[
    '_controller' => 'AppBundle:Race:list',
    'year' => date('Y'),
    'category' => ''
],[
    'year' => '\d+',
    'category' => 'all|mtb|szosa|przelaj'
]));
$collection->add('site_race_list', new Route('/wyniki/',[
    '_controller' => 'AppBundle:Race:list',
]));
$collection->add('site_page_show', new Route('/inne/{url}.html', [
    '_controller' => 'AppBundle:Page:show',
]));
$collection->add('site_race_add', new Route('/registration/{id}/', [
    '_controller' => 'AppBundle:Race:add',
    'id' => '\d+'
]));
$collection->add('site_race_teamsearch', new Route('/ajax/teamsearch/{path}', [
    '_controller' => 'AppBundle:Race:teamsearch',
]));

// PANEL
$collection->add('panel_user_merge', new Route('/panel/merge', [
    '_controller' => 'AppBundle:User:merge',
]));
$collection->add('panel_user_unmerge', new Route('/panel/unmerge/{player_id}', [
    '_controller' => 'AppBundle:User:unmerge',
]));
$collection->add('panel_user_list', new Route('/panel/list', [
    '_controller' => 'AppBundle:User:list',
]));
$collection->add('panel_user_upc', new Route('/panel/races', [
    '_controller' => 'AppBundle:User:upc',
]));

// ADMIN
$collection->add('site_admin_userList', new Route('/admin/user-list', [
    '_controller' => 'AppBundle:Admin:userList',
]));
$collection->add('site_admin_add', new Route('/admin/add', [
    '_controller' => 'AppBundle:Admin:add',
]));
$collection->add('site_admin_edit', new Route('/admin/edit/{id}', [
    '_controller' => 'AppBundle:Admin:edit',
    'id' => '\d+'
]));
$collection->add('site_admin_editFile', new Route('/admin/edit-file/{id}', [
    '_controller' => 'AppBundle:Admin:editFile',
    'id' => '\d+'
]));
$collection->add('site_admin_del', new Route('/admin/usun/{id}', [
    '_controller' => 'AppBundle:Admin:del',
    'id' => '\d+'
]));
$collection->add('site_admin_index', new Route('/admin/', [
    '_controller' => 'AppBundle:Admin:index',
]));



// AJAX
$collection->add('site_ajax_playerRegister', new Route('/ajax/playerRegister', [
    '_controller' => 'AppBundle:Ajax:playerRegister',
]));
$collection->add('site_ajax_playerRaceList', new Route('/ajax/playerRaceList', [
    '_controller' => 'AppBundle:Ajax:playerRaceList',
]));
$collection->add('site_ajax_playerRaceAll', new Route('/ajax/playerRaceAll', [
    '_controller' => 'AppBundle:Ajax:playerRaceAll',
]));
$collection->add('site_ajax_showResult', new Route('/ajax/showResult/{id}', [
    '_controller' => 'AppBundle:Ajax:showResult',
]));
$collection->add('site_ajax_userPlayerList', new Route('/ajax/userPlayerList', [
    '_controller' => 'AppBundle:Ajax:userPlayerList',
]));
$collection->add('site_ajax_playerChangeTeam', new Route('/ajax/playerChangeTeam', [
    '_controller' => 'AppBundle:Ajax:playerChangeTeam',
]));

// API
$collection->add('api_playerRaceAll', new Route('/api/{token}/player-race/{id}', [
    '_controller' => 'AppBundle:Api:playerRaceAll',
    'id' => '\d+'
]));
$collection->add('api_updateResult', new Route('/api/{token}/update-result', [
    '_controller' => 'AppBundle:Api:updateResult',
], [], [], '', [], ['POST']));

// PDF
$collection->add('pdf_confirmRegistration', new Route('/panel/pdf/potwierdzenie/{id}', [
    '_controller' => 'AppBundle:Pdf:confirmRegistration',
    'id' => '\d+'
]));
$collection->add('pdf_registeredPlayer', new Route('/panel/pdf/zapisani/{id}', [
    '_controller' => 'AppBundle:Pdf:registeredPlayer',
    'id' => '\d+'
]));
$collection->add('pdf_registeredPlayer_public', new Route('/pobierz/pdf/zapisani/{id}', [
    '_controller' => 'AppBundle:Pdf:registeredPlayerPublic',
    'id' => '\d+'
]));

return $collection;